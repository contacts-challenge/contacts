package com.gvv.contacts.cmd.repository.entity;

import javax.persistence.*;

@Entity
@Table(name = "contact_tbl")
public class ContactEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "public_id")
    private String publicId;

    private String name;

    private String mobile;

    private Boolean validated;

    public ContactEntity() {
    }

    public ContactEntity(String publicId, String name, String mobile, Boolean validated) {
        this.publicId = publicId;
        this.name = name;
        this.mobile = mobile;
        this.validated = validated;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPublicId() {
        return publicId;
    }

    public void setPublicId(String publicId) {
        this.publicId = publicId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Boolean getValidated() {
        return validated;
    }

    public void setValidated(Boolean validated) {
        this.validated = validated;
    }
}
