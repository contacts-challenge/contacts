package com.gvv.contacts.cmd.repository;

import com.gvv.contacts.cmd.repository.entity.ContactEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ContactRepository extends JpaRepository<ContactEntity, Integer> {
    Optional<ContactEntity> findByPublicId(String publicId);
}
