package com.gvv.contacts.cmd.repository;

import com.gvv.contacts.cmd.repository.entity.ScoreEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "score-api", url = "http://localhost:1987/score-api/v1")
public interface ScoreHTTPRepository {

    @GetMapping("/prospect/{publicId}/score")
    public ResponseEntity<ScoreEntity> getScore(@PathVariable String publicId);
}
