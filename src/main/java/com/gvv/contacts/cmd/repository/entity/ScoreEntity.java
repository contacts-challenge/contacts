package com.gvv.contacts.cmd.repository.entity;

public class ScoreEntity {

    private String publicId;
    private Integer score;

    public ScoreEntity() {
    }

    public ScoreEntity(String publicId, Integer score) {
        this.publicId = publicId;
        this.score = score;
    }

    public String getPublicId() {
        return publicId;
    }

    public void setPublicId(String publicId) {
        this.publicId = publicId;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }
}
