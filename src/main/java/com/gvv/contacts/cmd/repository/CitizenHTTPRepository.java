package com.gvv.contacts.cmd.repository;

import com.gvv.contacts.cmd.repository.entity.CitizenEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "citizen-api", url = "http://localhost:1986/citizen-api/v1")
public interface CitizenHTTPRepository {

    @PostMapping(value = "/citizen/{publicId}")
    public ResponseEntity<CitizenEntity> queryCitizen(CitizenEntity citizen);
}
