package com.gvv.contacts.cmd.repository.entity;

public class CitizenEntity {
    private String publicId;
    private String name;

    public CitizenEntity() {
    }

    public CitizenEntity(String publicId, String name) {
        this.setPublicId(publicId);
        this.setName(name);
    }

    public String getPublicId() {
        return publicId;
    }

    public void setPublicId(String publicId) {
        this.publicId = publicId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
