package com.gvv.contacts.cmd.repository;

import com.gvv.contacts.cmd.repository.entity.PersonEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "person-api", url = "http://localhost:1985/person-api/v1")
public interface PersonHTTPRepository {

    @GetMapping("/person/{publicId}/background")
    public ResponseEntity<PersonEntity> queryPersonByPublicId(@PathVariable String publicId);
}
