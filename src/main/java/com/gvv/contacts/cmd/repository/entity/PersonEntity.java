package com.gvv.contacts.cmd.repository.entity;

public class PersonEntity {
    private String publicId;
    private Boolean background;

    public PersonEntity() {
    }

    public PersonEntity(String publicId, Boolean background) {
        this.publicId = publicId;
        this.background = background;
    }

    public String getPublicId() {
        return publicId;
    }

    public void setPublicId(String publicId) {
        this.publicId = publicId;
    }

    public Boolean getBackground() {
        return background;
    }

    public void setBackground(Boolean background) {
        this.background = background;
    }
}
