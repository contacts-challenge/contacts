package com.gvv.contacts.cmd.dto;

public class ContactDTO {

    private String publicId;
    private String name;
    private String mobile;

    public ContactDTO() {
    }

    public ContactDTO(String publicId, String name, String mobile) {
        this.publicId = publicId;
        this.name = name;
        this.mobile = mobile;
    }

    public String getPublicId() {
        return publicId;
    }

    public void setPublicId(String publicId) {
        this.publicId = publicId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Override
    public String toString() {
        return "{" +
                "publicId='" + publicId + '\'' +
                ", name='" + name + '\'' +
                ", mobile='" + mobile + '\'' +
                '}';
    }
}
