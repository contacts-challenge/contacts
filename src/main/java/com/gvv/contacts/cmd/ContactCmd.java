package com.gvv.contacts.cmd;

import com.gvv.contacts.cmd.dto.ContactDTO;
import com.gvv.contacts.cmd.service.ContactService;
import io.vavr.control.Try;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ShellComponent
public class ContactCmd {

    private ContactService contactService;

    @Autowired
    public ContactCmd(ContactService contactService){
        this.contactService = contactService;
    }

    @ShellMethod(value = "Create a new contact.", key = "create")
    public String createContact(@ShellOption(
            value = {"-C", "--contact"},
            help = "Hints:\n" +
                    "\t\t\t- Fields supported in the following order: publicId name mobile\n" +
                    "\t\t\t- Use single quotes '' for composed names\n" +
                    "\t\tExamples:\n" +
                    "\t\t\t$ create -C 456 'Ruth Vargas' 1234567890",
            arity = 3) String[] fields) {

        return this.contactService.registerContact(new ContactDTO(fields[0], fields[1], fields[2]))
            .map(contactDTO -> CmdResponse.AWAITING_FOR_VALIDATION)
            .getOrElse(CmdResponse.CONTACT_ALREADY_EXISTS);
    }

    @ShellMethod(value = "Create contacts from a text file.", key = "fcreate")
    public String createContactsFromFile(@ShellOption(
        value = {"-F", "--file"}
    ) String fileName){
        File contactsFile = new File(fileName);
        Try<Stream<String>> lines = Try.of(() -> Files.lines(Paths.get(contactsFile.getPath())));

        if(lines.isFailure()){
            return CmdResponse.CANNOT_READ_INPUT_FILE;
        } else {
            return CmdResponse.AWAITING_FOR_VALIDATION + ": " + lines.get()
                .map(line -> {
                    String[] fields = line.split(",");
                    Try<ContactDTO> contactDTOTry = this.contactService.registerContact(new ContactDTO(fields[0], fields[1], fields[2]));
                    contactDTOTry.onFailure(failure -> System.out.println(CmdResponse.CONTACT_ALREADY_EXISTS));
                    return contactDTOTry;
                })
                .collect(Collectors.toList())
                .size();
        }
    }
}