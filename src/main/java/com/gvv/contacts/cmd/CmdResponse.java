package com.gvv.contacts.cmd;

public class CmdResponse {
    public static final String AWAITING_FOR_VALIDATION = "Awaiting for Validation.";
    public static final String CONTACT_ALREADY_EXISTS = "Contact already exists";
    public static final String CANNOT_READ_INPUT_FILE = "Cannot read input file";
}
