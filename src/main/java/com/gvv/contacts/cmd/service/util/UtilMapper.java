package com.gvv.contacts.cmd.service.util;

import com.gvv.contacts.cmd.dto.ContactDTO;
import com.gvv.contacts.cmd.repository.entity.ContactEntity;
import org.springframework.stereotype.Component;

@Component
public class UtilMapper {

    public ContactEntity toEntity(ContactDTO contactDTO){
        ContactEntity entity = new ContactEntity();
        entity.setPublicId(contactDTO.getPublicId());
        entity.setName(contactDTO.getName());
        entity.setMobile(contactDTO.getMobile());
        return entity;
    }

    public ContactDTO toDto(ContactEntity entity) {
        ContactDTO dto = new ContactDTO();
        dto.setPublicId(entity.getPublicId());
        dto.setName(entity.getName());
        dto.setMobile(entity.getMobile());
        return dto;
    }
}
