package com.gvv.contacts.cmd.service;

import com.gvv.contacts.cmd.dto.ContactDTO;
import com.gvv.contacts.cmd.repository.CitizenHTTPRepository;
import com.gvv.contacts.cmd.repository.ContactRepository;
import com.gvv.contacts.cmd.repository.PersonHTTPRepository;
import com.gvv.contacts.cmd.repository.ScoreHTTPRepository;
import com.gvv.contacts.cmd.repository.entity.CitizenEntity;
import com.gvv.contacts.cmd.repository.entity.ContactEntity;
import com.gvv.contacts.cmd.repository.entity.PersonEntity;
import com.gvv.contacts.cmd.repository.entity.ScoreEntity;
import com.gvv.contacts.cmd.service.exception.ContactAlreadyExistsException;
import com.gvv.contacts.cmd.service.util.UtilMapper;
import io.vavr.control.Try;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

@Service
public class ContactService {

    private ContactRepository contactRepository;
    private ScoreHTTPRepository scoreHTTPRepository;
    private CitizenHTTPRepository citizenHTTPRepository;
    private PersonHTTPRepository personHTTPRepository;
    private UtilMapper utilMapper;
    private ThreadPoolExecutor executor;


    @Autowired
    public ContactService(ContactRepository contactRepository,
                          UtilMapper utilMapper,
                          CitizenHTTPRepository citizenHTTPRepository,
                          PersonHTTPRepository personHTTPRepository,
                          ScoreHTTPRepository scoreHTTPRepository){
        this.contactRepository = contactRepository;
        this.utilMapper = utilMapper;
        this.citizenHTTPRepository = citizenHTTPRepository;
        this.personHTTPRepository = personHTTPRepository;
        this.scoreHTTPRepository = scoreHTTPRepository;
        this.executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);
    }

    public Try<ContactDTO> registerContact(ContactDTO contactDTO) {
        Optional<ContactEntity> entity = this.contactRepository.findByPublicId(contactDTO.getPublicId());
        if(entity.isPresent()){
            return Try.failure(new ContactAlreadyExistsException("The contact already exists. "));
        } else {
            CompletableFuture<ResponseEntity<CitizenEntity>> citizenFuture = CompletableFuture
                    .supplyAsync(() -> this.citizenHTTPRepository
                            .queryCitizen(new CitizenEntity(contactDTO.getPublicId(), contactDTO.getName())), executor);
            CompletableFuture<ResponseEntity<PersonEntity>> personFuture = CompletableFuture
                    .supplyAsync(() -> this.personHTTPRepository
                            .queryPersonByPublicId(contactDTO.getPublicId()), executor);

            CompletableFuture<Optional<ContactDTO>> combinedFuture = CompletableFuture
                    .allOf(citizenFuture, personFuture)
                    .thenApply(v -> {
                        HttpStatus status = citizenFuture.thenApply(ResponseEntity::getStatusCode).join();
                        PersonEntity person = personFuture.thenApply(HttpEntity::getBody).join();
                        if(HttpStatus.OK.equals(status) && !person.getBackground()){
                            return Optional.of(contactDTO);
                        } else {
                            return Optional.empty();
                        }
                    });

            combinedFuture.thenAccept(resolve-> {
               if(resolve.isPresent()){
                   ResponseEntity<ScoreEntity> responseScore = this.scoreHTTPRepository.getScore(resolve.get().getPublicId());
                   if(HttpStatus.NO_CONTENT.equals(responseScore.getStatusCode())){
                       System.out.println("The contact was not found in the scoring system");
                   } else {
                       ScoreEntity scoreEntity = responseScore.getBody();
                       if(scoreEntity.getScore() <= 50){
                           System.out.println("The person does not have enough scoring");
                       } else {
                           Optional<ContactEntity> futureEntityOpt = this.contactRepository.findByPublicId(resolve.get().getPublicId());
                           if(futureEntityOpt.isPresent()){
                               ContactEntity futureEntity = futureEntityOpt.get();
                               futureEntity.setValidated(true);
                               this.contactRepository.save(futureEntity);
                               System.out.println("The contact has been added to the trusted list");
                           }
                       }
                   }
               } else {
                   System.out.println("it is not a valid contact");
               }
            });

            ContactEntity newContactEntity = this.utilMapper.toEntity(contactDTO);
            newContactEntity.setValidated(false);
            return Try.success(this.utilMapper.toDto(this.contactRepository.save(newContactEntity)));
        }
    }
}
