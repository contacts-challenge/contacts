package com.gvv.contacts.cmd.service.exception;

public class ContactAlreadyExistsException extends Exception {
    public ContactAlreadyExistsException(String message, Throwable throwable){
        super(message, throwable);
    }

    public ContactAlreadyExistsException(String message){
        super(message);
    }
}
