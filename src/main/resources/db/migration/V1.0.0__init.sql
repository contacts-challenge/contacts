CREATE TABLE contact_tbl (
   id IDENTITY PRIMARY KEY,
   public_id VARCHAR(15) UNIQUE NOT NULL,
   name VARCHAR(50) NOT NULL,
   mobile VARCHAR(20) NOT NULL,
   validated BOOLEAN
);

INSERT INTO contact_tbl (public_id, name, mobile, validated)VALUES (80759644, 'german', '3183703622', false);
