package com.gvv.contacts.cmd;

import com.gvv.contacts.cmd.dto.ContactDTO;
import com.gvv.contacts.cmd.service.ContactService;
import com.gvv.contacts.cmd.service.exception.ContactAlreadyExistsException;
import io.vavr.control.Try;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class ContactCommandTest {

	@Mock
	private ContactService contactService;

	@InjectMocks
	private ContactCmd contactCmd;

	@Before
	public void init(){
		MockitoAnnotations.initMocks(contactCmd);
	}

	@Test
	public void shouldCreateAContactInAwaitingStatusTest() throws ContactAlreadyExistsException {
		given(contactService.registerContact(any()))
				.willReturn(Try.success(mockContactDTO()));

		//when
		String outcome = this.contactCmd.createContact(mockFields());

		//then
		Assert.assertEquals(CmdResponse.AWAITING_FOR_VALIDATION, outcome);
	}

	private static String[] mockFields(){
		return new String[] {"123", "german", "1234567890"};
	}

	private static ContactDTO mockContactDTO(){
		return new ContactDTO("123", "german", "1234567890");
	}
}
