package com.gvv.contacts.service;

import com.gvv.contacts.cmd.dto.ContactDTO;
import com.gvv.contacts.cmd.repository.ContactRepository;
import com.gvv.contacts.cmd.repository.entity.ContactEntity;
import com.gvv.contacts.cmd.service.ContactService;
import com.gvv.contacts.cmd.service.exception.ContactAlreadyExistsException;
import com.gvv.contacts.cmd.service.util.UtilMapper;
import io.vavr.control.Try;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class ContactServiceTest {

    @Mock
    private UtilMapper mapper;

    @Mock
    private ContactRepository contactRepository;

    @InjectMocks
    private ContactService contactService;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(contactService);
    }

    @Test
    public void shouldRegisterANewContactTest(){
        given(this.mapper.toEntity(any())).willReturn(mockContactEntity());
        given(this.contactRepository.save(any())).willReturn(mockContactEntity());
        given(this.mapper.toDto(any())).willReturn(mockContactDTO());

        //when
        Try<ContactDTO> outcome = this.contactService.registerContact(mockContactDTO());

        //then
        Assert.assertEquals(mockContactDTO().toString(), outcome.get().toString());
    }

    @Test
    public void shouldThrownExceptionWhenContactAlreadyExistsTest() {
        given(this.contactRepository.findByPublicId("123")).willReturn(Optional.of(mockContactEntity()));

        //when
        Try<ContactDTO> outcome = this.contactService.registerContact(mockContactDTO());

        //then
        Assert.assertTrue(outcome.isFailure());
    }

    private static ContactEntity mockContactEntity() {
        ContactEntity entity = new ContactEntity("123", "german", "1234567890", false);
        entity.setId(1);
        return entity;
    }

    private static ContactDTO mockContactDTO(){
        return new ContactDTO("123", "german", "1234567890");
    }
}
