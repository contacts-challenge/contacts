## Contacts API

### Install the following tools

- jdk 1.8
- maven 3.5

### Getting Started

Make sure to configure Citizen, Person and Score mock apis, as stated on:

- https://gitlab.com/contacts-challenge/citizen-api/blob/master/README.md

- https://gitlab.com/contacts-challenge/person-bg/blob/master/README.md

- https://gitlab.com/contacts-challenge/score-api/blob/master/README.md

Generate the binary distribution:

`$ mvn clean package install`

Run the application

```
$ cd target
$ java -jar contacts-0.0.1-SNAPSHOT.jar
```

### Interacting With The Command Line

This is a spring boot application with exposes a shell to register contacts.
The following are the available commands

- Register a single contact:

```
$ create -C 456 'Ruth Vargas' 1234567890
```

- Register a list of contacts from a text
  (**There is an example of an input file in** PROJECT_ROOT_FOLDER/src/test/resources/input.txt)

```
$ fcreate /ABSOLUTE_PATH_TO_THE_INPUT_FILE/input.txt
```

- To get Help about the commands

```
$ help create
$ help fcreate
```

- The application comes with an embedded database (H2), so you can
go to http://localhost:1984/h2, to verify which contacts have been successfully
added to the trusted list, by executing the following query:

`SELECT * FROM CONTACT_TBL WHERE VALIDATED = TRUE`

**When logging to the h2 web console, make sure that the 'JDBC URL' field is set as jdbc:h2:mem:contacts-db**